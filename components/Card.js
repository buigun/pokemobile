import React from 'react'
import { View, StyleSheet, Image, TouchableOpacity, Text, Button } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'

export default function Card({ pokemon, keyval, navigation }) {
    return (
        <View style={styles.container}>
            <Image source={{uri:`https://pokeres.bastionbot.org/images/pokemon/${keyval+1}.png`}} style={{height:300}} />
            <View style={styles.descContainer}>
                <View style={styles.videoDetails}>
                    <Text numberOfLines={2} style={styles.videoTitle}>{pokemon.name}</Text>
                </View>
            </View>
            <View style={styles.buttonGroup}>
                <View style={{width: 120}}>
                    <Button onPress={()=>navigation.navigate("Detail",{id: keyval+1})} title="DETAIL" color="#E8505B"></Button>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        padding: 15,
        borderColor: 'grey',
        borderStyle: 'solid',
        borderBottomWidth: 0.5,
        backgroundColor: "white"
    },
    descContainer: {
        flexDirection: "row",
        paddingTop: 15,
    },
    videoTitle: {
        fontSize: 20,
        color: "#3c3c3c",
    },
    videoDetails: {
        paddingHorizontal: 15,
        flex: 1
    },
    buttonGroup: {
        flexDirection: "row",
        marginTop: 16,
    }
})