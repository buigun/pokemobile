import React from 'react';
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'

import { Provider } from 'react-redux'
import store from './stores';

import Login from './screens/Login'
import Home from './screens/Home'
import About from './screens/About'
import Detail from './screens/Detail'

const Stack = createStackNavigator()

export default function App() {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen name="Login" options={{headerShown: false}} component={Login}></Stack.Screen>
          <Stack.Screen name="Home" options={{headerShown: false}} component={Home}></Stack.Screen>
          <Stack.Screen name="Detail" component={Detail}></Stack.Screen>
          <Stack.Screen name="About" options={{headerShown: false}} component={About}></Stack.Screen>
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}
