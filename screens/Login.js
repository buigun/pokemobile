import React,{ useState } from 'react'
import { StyleSheet, View, Button, TextInput, Image, Text } from 'react-native'

export default function SignUp({ navigation }) {
    const [name, setName] = useState("")

    return (
        <View style={styles.container}>
            <Image style={{height: 100, width: 100, marginBottom: 20 }} source={require("../assets/logo.png")}/>
            <Text style={{fontSize:22, fontWeight:"bold"}}>Welcome to PokeMobile</Text>
            <Text style={{marginTop:40}}>Please enter your name</Text>
            <TextInput style={styles.input} onChangeText={(input)=>setName(input)} placeholder="your name" textContentType="username"></TextInput>
            <View style={{marginTop: 40, width: '80%'}}>
                <Button onPress={()=>navigation.navigate("Home",{name})} title="ENTER" color="#E8505B"></Button>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex : 1,
        alignItems: "center",
        justifyContent: "center"
    },
    form:{
        marginTop: 40
    },
    input: {
        borderWidth: 1,
        borderColor: '#393E46',
        paddingLeft: 16,
        padding: 8,
        width: '80%',
        borderRadius: 5,
        marginTop: 16,
    }
})