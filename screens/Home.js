import React,{useState,useEffect} from 'react';
import { View, StyleSheet, Image, TouchableOpacity, Text, ScrollView } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'
import Card from '../components/Card'

import { useSelector, useDispatch} from 'react-redux'
import { getPokemons } from '../stores/actions/pokemonAction'

export default function Home({ navigation, route }) {
    const { name } = route.params
    const [number, setnumber] = useState(6)

    const pokemons = useSelector((state)=> state.pokemonReducer.pokemons)
    const dispatch = useDispatch()

    let cards

    if (pokemons) {
        cards = pokemons.results.map((val, key) => {
            return <Card 
                key={key} keyval={key} pokemon={val} navigation={navigation}
            />
        })
    } else {
        return []
    }

    useEffect(()=>{
        dispatch(getPokemons(number))
    },[number,dispatch])

    return (
        <View style={styles.container}>
            <View style={styles.navbar}>
                <Icon style={styles.navItem} name="account-circle" size={25} />
                <Text style={styles.navItem}>Hello {name} </Text>
            </View>
            <View style={styles.body}>
                <ScrollView>
                    {pokemons == undefined ?
                        <Image style={{height: 64, width: 64, marginBottom: 20 }} source={require("../assets/loading.gif")}/> :
                        cards
                    }
                </ScrollView>
            </View>
            <View style={styles.tabBar}>
                <TouchableOpacity style={styles.tabItem}
                    onPress={()=>navigation.navigate("Home")}
                >
                    <Icon name="home" size={25} color={'#E8505B'} />
                    <Text style={styles.tabTitle, {color: '#E8505B'}}>Home</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.tabItem}
                    onPress={()=>navigation.navigate("About")}
                >
                    <Icon name="info" size={25} />
                    <Text style={styles.tabTitle}>About</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    navbar: {
        height: 80,
        backgroundColor: "white",
        elevation: 3,
        paddingHorizontal: 15,
        flexDirection: "row",
        alignItems: "center"
    },
    rightNav: {
       flexDirection: "row",
    },
    navItem: {
        marginTop: 24,
        marginLeft: 8
    },
    body:{
        flex: 1,
    },
    tabBar: {
        backgroundColor: "white",
        height: 60,
        borderTopWidth: 0.5,
        borderColor: "#e5e5e5",
        flexDirection: "row",
        justifyContent: "space-around"
    },
    tabItem: {
        alignItems: "center",
        justifyContent: "center"
    },
    tabTitle: {
        fontSize: 11,
        color: "#3c3c3c",
        paddingTop: 4
    }
})