import React,{useEffect} from 'react'
import { StyleSheet, View, Image, Text, ScrollView} from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'

import { useSelector, useDispatch} from 'react-redux'
import { getPokemonDetail } from '../stores/actions/pokemonAction'

export default function Detail({ navigation, route }) {
    const { id } = route.params
    const detail = useSelector((state)=> state.pokemonReducer.pokemon)
    const dispatch = useDispatch()

    useEffect(()=>{
        dispatch(getPokemonDetail(id))
    },[dispatch,id])

    return (
        <View style={styles.container}>
            <View >
                <ScrollView>
                    {!detail && <Image style={{height: 64, width: 64, marginBottom: 20 }} source={require("../assets/loading.gif")}/>}
                    {detail && <View>
                        <Image source={{uri:`https://pokeres.bastionbot.org/images/pokemon/${detail.id}.png`}} style={{height:300}} />
                        <Text style={{fontSize: 24, marginLeft: 5, fontWeight:"bold"}}> {detail.name} </Text>
                        <Text style={{fontSize: 18, marginLeft: 10, marginTop: 10}}>height: {detail.height/10} meter(s) </Text>    
                        <Text style={{fontSize: 18, marginLeft: 10, marginTop: 10}}>weight: {detail.weight/10} kg(s) </Text> 
                    </View>}
                </ScrollView>
                {/* <Image style={{height: 80, width: 80, marginBottom: 20 }} source={require("../assets/logo.png")}/>
                <Text> {JSON.stringify(detail)} </Text> */}
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex : 1,
        backgroundColor: 'white'
    },
    body: {
        flex : 1,
        alignItems: "center",
        justifyContent: "center"
    }
})